package ru.pcs.web.forms;

import lombok.Data;

@Data
public class ProductForm {
    private Integer id;
    private String description;
    private Integer price;
    private Integer amount;
}
