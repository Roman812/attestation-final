package ru.pcs.web.services;

import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductsService {
    void addProduct(ProductForm form);
    List<Product> getAllProducts();
    void deleteProduct(Integer productId);
    void updateProduct(Integer productId, ProductForm form);
    Product getProduct(Integer productId);
}
