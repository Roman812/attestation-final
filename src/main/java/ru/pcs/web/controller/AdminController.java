package ru.pcs.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller

public class AdminController {

    @GetMapping("/admin")
    public String getIndexPage(){
        return "admin";
    }
}
