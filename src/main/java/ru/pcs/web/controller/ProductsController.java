package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.ProductsService;
import java.util.List;

@Controller
public class ProductsController {

    private final ProductsService productsService;

    @Autowired
    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/guns")
    public String getIndexPage(){
        return "index";
    }

    @GetMapping("/products")
    public String getProductsPage(Model model) {
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @GetMapping("/productsAdmin")
    public String getAdminProductsPage(Model model) {
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        return "productsAdmin";
    }

    @PostMapping("/productsAdmin")
    public String addProduct(ProductForm form) {
        productsService.addProduct(form);
        return "redirect:/productsAdmin";
    }

    @PostMapping("/productsAdmin/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productsService.deleteProduct(productId);
        return "redirect:/productsAdmin";
    }

    @GetMapping("/productsAdmin/{product-id}")
    public String getProductUpdatePage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "productUpdate";

    }

    @GetMapping("/productsAdmin/{product-id}/update")
    public String update(@PathVariable("product-id") Integer productId, ProductForm form) {
        productsService.updateProduct(productId, form);
        return "redirect:/productsAdmin/{product-id}";
    }
}
